# TP Kubernetes

## 1. Namespace et Ressources Quota

Lors de la création d'un node avec minikube, il y a 4 namespaces créé.
- default : le namespace par défaut.
- kube-system : le namespace pour les objets créé par le système Kubernetes
- kube-node-lease : 
- kube-public : le namespace créé automatiquement et disponible pour tous les utilisateurs.

Dans notre cluster, afin de découper les tâches, nous avons créé 3 namespaces supplémentaires :
- bdd
- production
- development

La commandes kubectl suivante nous permet de lister les namepaces disponibles dans le cluster :
`kubectl get namespaces --show-labels`

Output :

NAME                   STATUS   AGE   LABELS
bdd                    Active   18h   kubernetes.io/metadata.name=bdd
default                Active   59d   kubernetes.io/metadata.name=default
development            Active   18h   kubernetes.io/metadata.name=development
ingress-nginx          Active   58d   app.kubernetes.io/instance=ingress-nginx,app.kubernetes.io/name=ingress-nginx,kubernetes.io/metadata.name=ingress-nginx
kube-node-lease        Active   59d   kubernetes.io/metadata.name=kube-node-lease
kube-public            Active   59d   kubernetes.io/metadata.name=kube-public
kube-system            Active   59d   kubernetes.io/metadata.name=kube-system
kubernetes-dashboard   Active   16h   addonmanager.kubernetes.io/mode=Reconcile,kubernetes.io/metadata.name=kubernetes-dashboard,kubernetes.io/minikube-addons=dashboard
production             Active   18h   kubernetes.io/metadata.name=production

Des ressources sont attribués a chaque namespaces.

Les quotas sont représentés en request qui est la demande de ressource du namespace et limits qui est l'utilisation limite des ressources du namespace.

- Le namespace development : 
    <b>requests.cpu</b> : le nombre total de demande d'UC pour tous les conteneurs ne doit pas dépasser 1UC
    <b>requests.memory</b> : la demande de mémoire à 1Go 
    <b>limits.cpu</b> : limite de cpu utilisable par le namespace
    <b>limits.memory</b> : limit de mémoire utilisable par le namespace

- Le namespace production : 
    <b>requests.cpu</b> : 1
    <b>requests.memory</b> : 1Go
    <b>limits.cpu</b> : 2
    <b>limits.memory</b> : 2Gi

- Le namespace bdd :
    <b>requests.cpu</b> : 2
    <b>requests.memory</b> : 1Go
    <b>limits.cpu</b> : 2
    <b>limits.memory</b> : 2Gi


## Pods et déploiements

A la création des pods, il y a deux façon d'explicitement indiquer à kubernetes dans quel namespace l'on souhaite ajouter le pod.

1. Avec la commande kubectl :

 `kubectl apply -f pod.yaml --namespace= <nom_du_namespace>`

2. Lors de la création du fichier yaml dans la section metadata :

`namespace: <nom_du_namespace>`

Lorsque l'on déploie avec Kubernetes, nous construisons un cluster.
C'est un "lot" de machines physique ou virtuelles qu'on apelle **nodes**
Ces mêmes **nodes hebergent les pods**. 
Lors d'un déploiement, on initilise les **conteneurs** contenus dans des **pods** qui sont eux même contenu dans des **replica set** qui nous servent à répliquer les pods.
Le **control plane** est un composant important d'un cluster kubernetes car il gère les nodes et les pods, à savoir qu'il prend des décisions globales pour répondre au évenements du cluster (démarrer par exemple un nouveau pod lorsque le réplica n'est plus satisfait)

Afin de créer les déploiements dans le bon namespace (contexte)

Pour connaitre le namespace dans lequel on se trouve : `kubectl config current-context`

Récuperer le nom du cluster et du user du current context `kubectl config view`

Ajouter un contexte : `kubectl config set-context development --namespace=development --cluster=minikube --user=minikube`

Enfin pour switcher vers le namespace désiré : `kubectl config use-context <nom_du_namespace>`

Sans cela le déploiements se crée dans le contexte par défaut qui s'apelle **default**


Pour créer un déploiement dans le contexte development : `kubectl apply -f deployment-devops303.yaml`

Si l'on tape la commande `kubectl get deployment` dans le contexte production, nous voyons le déploiement **hello-world** mais nous ne voyons pas le déploiement devops-303, les namespaces nous permettent de rajouter une sécurité au cluster et de découper les ressources.

De plus, les variables d'environnements utilisables pour la base de données sont indiquées dans un object Kubernetes apellé **Config Map**. Cet objet à la même but que l'objet **Secret** qui donne aujourd'hui le même niveau de sécurité.

## Services et Ingress


Les services nous permettent d'exposer une ressource à l'extérieur de notre cluster. Par défault si nous ne spécifions rien, le ServiceType utilisé est **cluster IP**

Tout d'abord il est possible de faire du port-forwarding depuis un pod, déploiement et même un service.

Ex : 

`kubectl get pod <nom_du_pod> -n <nom_du_namespace>  --template='{{(index (index .spec.containers 0).ports 0).containerPort}}'`

Output : `80`

On tape ensuite la commande qui nous permettra de bind le port désigné plus haut et le port du container.

`kubectl port-forward hello-world-7dcdb86496-dj94z 80:80`

Dans un navigateur tapez : 127.0.0.1:80 pour acceder à la ressource.

Il existe d'autres ServicesTypes tel que :

- NodePort
- LoadBalancer
- ExternalName

Cluster IP : expose le service sur une IP stable **interne** au cluster.

Chaque pods détient sa propre adresse IP qui est défini à l'intérieur d'un node.
Il est necéssaire de se rapeller que les pods sont éphémère et que par définition, il ont une durée de vie qui n'est pas définie. On ne peut donc pas joindre les pods directement avec leur adresse IP. Un service nous permet de définir une **adresse IP stable** qui nous permet de joindre facilement les ressources des pods.

Afin d'être accessible depuis un navigateur, nous créons une ressource Ingress.
Voici le schéma que nous avons reproduit :

![Kubernetes Cluster IP Service](/assets/images/cluster_ip_service.png)

Nous créons la ressource service dans le namespace "production" comme suit :

`kubectl apply -f service.yml`

D'autres part K8s nous crée une ressource Endpoints a chaque service crée qui nous permet de visualiser les pods gérés par le service

`kubectl get endpoints`

Output :

NAME        ENDPOINTS                                   AGE
myservice   172.17.0.4:80,172.17.0.6:80,172.17.0.8:80   76m

Il y a plusieurs façon d'installer le module Ingress, K8s nous met à disposition un controlleur Ingress que l'on
peut activer avec :

`minikube addons enable ingress`

Cela a pour effet de créer un pod qui se chargera de communique avec le service ingress. On peut voir ce pod en tapant : `kubectl get pods -n kube-system`

On crée un service de type **Load balancer** qui permettra de partager et gerer les ressources vers les pods.

`kubectl expose deployment hello-world --type=LoadBalancer --name=my-service-load-balancer` créé dans le namespace production

![Kubernetes Cluster IP Ingress](/assets/images/hello-world-domaine-simule1.png)

![Kubernetes Cluster IP Ingress](/assets/images/hello-world-domaine-simule2.png)



`kubectl expose deployment devops-303 --type=LoadBalancer --name=my-service-load-balancer` créé dans le namespace development

Nous modifions le fichier host afin de joindre les ressources depuis deux noms de

## Superviser le cluster

Pour superviser le cluster, on utilise Prometheus et Graphana.

`kubectl apply --filename https://raw.githubusercontent.com/giantswarm/prometheus/master/manifests-all.yaml`

pour en savoir plus sur les pods, services et déploiements créé, on tape :

`kubectl get pods,svc,replicaset,deploy -n monitoring`

Pods crées :

- pod/alertmanager-7bd9f64bbb-wtnk9
- pod/grafana-core-7bb484fdf9-82bkl
- pod/grafana-import-dashboards--1-6hcpl
- pod/kube-state-metrics-758cdbb755-nxswl
- pod/node-directory-size-metrics-mhsd
- pod/prometheus-core-6f99b67bd4-br8t8
- pod/prometheus-node-exporter-sxsmm

Service :

- service/alertmanager
- service/grafana
- service/kube-state-metrics  
- service/prometheus-node-exporter

Recipla Set: 

- replicaset.apps/alertmanager-7bd9f64bbb
- replicaset.apps/grafana-core-7bb484fdf9 
- replicaset.apps/kube-state-metrics-758cdbb755
- replicaset.apps/prometheus-core-6f99b67bd4

Deploiement :
- deployment.apps/alertmanager 
- deployment.apps/grafana-core
- deployment.apps/kube-state-metrics
- deployment.apps/prometheus-core

## RBAC : role et roleBindings

Les roles sur k8s permettent de restreindre l'accès à des ressources, namespace, pods ect.. selon l'utilisateur authentifié.

Par défaut k8s nous laisse l'accès à tous le cluster et ne restreint aucun accès. Il est possible de créer un utilisateur et lui assigner un **role**

Etape 1 : création d'une clé et d'un certificat

`openssl genrsa -out user1.key 2048`

`openssl req -new -key user1.key -out user1.csr -subj “/CN=user1/O=group1”`

`openssl x509 -req -in user1.csr -CA ~/.minikube/ca.crt -CAkey ~/.minikube/ca.key -CAcreateserial -out user1.crt -days 500`

Etape 2 : création d'un user et d'un contexte user

`kubectl config set-credentials user1 --client-certificate=user1.crt --client-key=user1.key`

`$ kubectl config set-context user1-context --cluster=minikube --user=user1`

Etape 3 : Donner des accès à l'utilisateur

Création d'un role : ce fichier permet de définir quelles ressources pourra acceder l'utilisateur. Nous créons un fichier role.yaml (dans le dossier role)

Création d'un binding role : ce fichier permet de lier un role avec un utilisateur. Nous créons un fichier role-binding.yaml (dans le dossier role)

Etape 4: Application des fichiers dans le namespace default

`kubectl config use-context minikube`

`kubectl apply -f role.yaml`

`kubectl apply -f role-binding.yaml`










